package com.crio.qcalc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

public class StandardCalculatorTest {
    private StandardCalculator standardCalculator;


    
    @BeforeEach
    void setup() {
        standardCalculator = new StandardCalculator();
    }

    @Test
    @DisplayName("Test Addition of Two Integers")
    void testAdditionOperation() {
        standardCalculator.add(1, 1);
        double actualResult = standardCalculator.getResult();
        Assertions.assertEquals(2, actualResult);
    }

    @Test
    @DisplayName("Test Substraction of Two Integers")
    void testSubtractionOperation() {
        standardCalculator.subtract(1, 1);
        double actualResult = standardCalculator.getResult();
        Assertions.assertEquals(0, actualResult);
    }

    @Test
    @DisplayName("Test divide of Two Integers")
    void testdivideOperation() {
        standardCalculator.divide(4, 2);
        double actualResult = standardCalculator.getResult();
        Assertions.assertEquals(2, actualResult);
    }

    @Test
    @DisplayName("Test multiply of Two Integers")
    void testmultiplyOperation() {
        standardCalculator.multiply(4, 2);
        double actualResult = standardCalculator.getResult();
        Assertions.assertEquals(8, actualResult);
    }

    @Test
    @DisplayName("Test Addition of Two Doubles")
    void testAdditionOperationForDoubles() {
        standardCalculator.add(1.0, 1.5);
        double actualResult = standardCalculator.getResult();
        Assertions.assertEquals(2.5, actualResult);
    }

    @Test
    @DisplayName("Test Subtraction of Two Doubles")
    void testSubtractionOperationForDoubles() {
        standardCalculator.subtract(10.0, 20.5);
        double actualResult = standardCalculator.getResult();
        Assertions.assertEquals(-10.5, actualResult);
    }

    @Test
    @DisplayName("Test Multiplication of Two Doubles")
    void testMultiplicationOperationForDoubles() {
        standardCalculator.multiply(2.2, 2);
        double actualResult = standardCalculator.getResult();
        Assertions.assertEquals(4.4, actualResult);
    }

    @Test
    @DisplayName("Test Division of Two Doubles")
    void testDivisionOperationForDoubles() {
        standardCalculator.divide(4.4, 2);
        double actualResult = standardCalculator.getResult();
        Assertions.assertEquals(2.2, actualResult);
    }

    @Test
    @DisplayName("Test Addition Overflow of Two Doubles")
    void testAdditionOverflowForDoubles() {
        Assertions.assertThrows(ArithmeticException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                standardCalculator.add(Double.MAX_VALUE, Double.MAX_VALUE);
            }
        });
    }

    @Test
    @DisplayName("Test Substraction Overflow of Two Doubles")
    void testSubstractionOverflowForDoubles() {
        Assertions.assertThrows(ArithmeticException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                standardCalculator.subtract(-Double.MAX_VALUE, Double.MAX_VALUE);
            }
        });
    }

}
